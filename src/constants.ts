export const NETWORKS = {
    STAGENET: {
        url: 'https://nodes-stagenet.wavesnodes.com',
        chainId: 'S',
        faucet: 'https://wavesexplorer.com/stagenet/faucet',
        explorer: 'https://wavesexplorer.com/stagenet'
    },
    TESTNET: {
        url: 'https://nodes-testnet.wavesnodes.com',
        chainId: 'T',
        faucet: 'https://wavesexplorer.com/testnet/faucet',
        explorer: 'https://wavesexplorer.com/testnet'
    },
    MAINNET: {
        url: 'https://nodes.wavesplatform.com',
        chainId: 'W',
        explorer: 'https://wavesexplorer.com'
    },
    CUSTOM: {
        url: 'https://node1.brickprotocol.com',
        chainId: 'B',
        explorer: 'https://explorer.brickprotocol.com'
    }
};
